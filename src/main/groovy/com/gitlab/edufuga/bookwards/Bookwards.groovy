package com.gitlab.edufuga.bookwards

import com.gitlab.edufuga.bookwards.bookreader.BookReader
import com.gitlab.edufuga.bookwards.questioner.Questioner
import com.gitlab.edufuga.wordlines.core.Status
import com.gitlab.edufuga.wordlines.documentprocessor.DocumentProcessor
import com.gitlab.edufuga.wordlines.sentencesreader.SentencesReader
import groovy.transform.CompileStatic

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

@CompileStatic
class Bookwards {
   final Path book
   final Path languageVocabulary
   final Path vocabularyRoot
   final Path sentences
   final Path bookWords
   final Path bookWordsInOrder

   final Path importedBook

   Bookwards(Path book) {
      this.book = book

      /* Import the book */
      String language = book.getParent().getParent().getFileName().toString()
      println "Detected language: $language"

      println "Importing the book $book into the corresponding folder for the language $language."

      BookImporter importer = new BookImporter(book, language)
      importedBook = importer.importBook()

      /* Process the imported book (generate the needed files for reading and questioning/reviewing) */
      // FIXME: This reuses the old structure of the code. The DocumentProcessor itself has to be refactored as well.

      if (importer.isImported()) {
         println "Splitting the sentences of the document."
         String[] fakedArgs = [importedBook.toAbsolutePath()]
         DocumentProcessor.main(fakedArgs)
      }

      /* Read the imported and processed book */
      String vocabularyRootAsString = System.getProperty("bookwards.vocabulary")
      if (vocabularyRootAsString == null || vocabularyRootAsString.isEmpty()) {
         Map<String, String> env = System.getenv()
         if (!env.containsKey("WORDSFILES")) {
            throw new RuntimeException("Neither the system property 'bookwards.vocabulary' " +
                    "nor the environment variable 'WORDSFILES' was set! You have to tell me where your vocabulary " +
                    "is found.")
         }
         vocabularyRootAsString = env.get("WORDSFILES")
      }

      vocabularyRoot = Paths.get(vocabularyRootAsString)
      println "Vocabulary root: $vocabularyRoot"

      languageVocabulary = vocabularyRoot.resolve("status").resolve(language)
      Files.createDirectories(languageVocabulary)

      sentences = importedBook.getParent().resolve("book")
      bookWords = sentences.resolve("metadata.tsv")
      if (Files.notExists(bookWords)) {
         throw new RuntimeException("The words file doesn't exist.")
      }
      bookWordsInOrder = importedBook.getParent().resolve("book_words_in_order.txt")
      if (Files.notExists(bookWordsInOrder)) {
         throw new RuntimeException("The words in order file doesn't exist.")
      }
   }

   void run() {
      read (importedBook.toAbsolutePath().toString(), languageVocabulary.toAbsolutePath().toString())
   }

   void question() {
      String words = bookWords.toAbsolutePath().toString()
      String statusFolder = languageVocabulary.toAbsolutePath().toString()
      String sentencesFolder = sentences.toAbsolutePath().toString()

      question(words, statusFolder, sentencesFolder)
   }

   void review(String status) {
      String words = bookWords.toAbsolutePath().toString()
      String statusFolder = languageVocabulary.toAbsolutePath().toString()
      String sentencesFolder = sentences.toAbsolutePath().toString()
      review(words, statusFolder, sentencesFolder, status)
   }

   void questionInBookOrder() {
      String words = bookWordsInOrder.toAbsolutePath().toString()
      String statusFolder = languageVocabulary.toAbsolutePath().toString()
      String sentencesFolder = sentences.toAbsolutePath().toString()

      question(words, statusFolder, sentencesFolder)
   }

   void reviewInBookOrder(String status) {
      String words = bookWordsInOrder.toAbsolutePath().toString()
      String statusFolder = languageVocabulary.toAbsolutePath().toString()
      String sentencesFolder = sentences.toAbsolutePath().toString()
      review(words, statusFolder, sentencesFolder, status)
   }

   static void read(String book, String statusFolder) {
      def bookFile = new File(book)
      if (!bookFile.exists()) {
         throw new RuntimeException("The given book file does not exist.")
      }

      println "Working with book '$bookFile.absolutePath'."
      Path bookPath = bookFile.toPath()

      println "Records will be read from status folder '$statusFolder'."

      def connectionsFolder = Paths.get(statusFolder).getParent().getParent().resolve("connections").toAbsolutePath().toString()
      println "Connections folder: $connectionsFolder"

      String bookName = bookPath.getFileName().toString().split("\\.").first()
      println "Book name: $bookName"
      Path sentencesFolder = bookPath.getParent().resolve(bookName)
      println "Sentences folder: $sentencesFolder"

      String language = Paths.get(statusFolder).getFileName()
      println "Language: $language"

      Path unitsFolder = Paths.get(statusFolder).getParent().getParent().resolve("units").resolve(language).toAbsolutePath()
      println "Units folder: $unitsFolder"

      def reader = new BookReader(bookPath, statusFolder, sentencesFolder, connectionsFolder, unitsFolder)
      reader.read()
   }

   static void question (String words, String statusFolder, String sentencesFolder) {
      def wordsFile = new File(words)
      if (!wordsFile.exists()) {
         println "The given file does not exist."
         return
      }
      println "Working with words file '$wordsFile.absolutePath'."
      Path wordsPath = wordsFile.toPath()

      println "Results will be saved in status folder '$statusFolder'."

      SentencesReader sentencesReader = new SentencesReader(new File(sentencesFolder))

      def questioner = new Questioner(wordsPath, sentencesReader, statusFolder, Paths.get(sentencesFolder))
      questioner.questionWords()
   }

   static void review (String words, String statusFolder, String sentencesFolder, String status) {
      def wordsFile = new File(words)
      if (!wordsFile.exists()) {
         println "The given file does not exist."
         return
      }
      println "Working with words file '$wordsFile.absolutePath'."
      Path wordsPath = wordsFile.toPath()

      println "Results will be saved in status folder '$statusFolder'."

      SentencesReader sentencesReader = new SentencesReader(new File(sentencesFolder))

      def questioner = new Questioner(wordsPath, sentencesReader, statusFolder, Paths.get(sentencesFolder))

      Status statusToQuestion = Status.valueOf(status)
      questioner.reviewWords(statusToQuestion)
   }

   static void askStateAndReview(Path book) {
      println "Do you want to review UNKNOWN, GUESSED, COMPREHENDED, KNOWN, WELL_KNOWN or MASTERED words? (Press ENTER after selection)"

      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      String status = reader.readLine();

      println "Do you want to review in book order (y/n)?"
      char inOrderChar = reader.read()
      boolean inOrder = (inOrderChar == ('y' as char))

      Bookwards bookwards = new Bookwards(book)
      if (inOrder) {
         bookwards.reviewInBookOrder(status)
      }
      else {
         bookwards.review(status)
      }
   }

   static void askOrderAndQuestion(Path book) {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

      println "Do you want to question the words in book order (y/n)?"
      char inOrderChar = reader.read()
      boolean inOrder = (inOrderChar == ('y' as char))

      Bookwards bookwards = new Bookwards(book)
      if (inOrder) {
         bookwards.questionInBookOrder()
      }
      else {
         bookwards.question()
      }
   }

   static void main(String[] args) {
      if (args.size() == 0) {
         println "Like this I won't do anything. You have to give me the absolute path to the book."
         return
      }

      if (args.size() == 1) {
         Path book = Paths.get(args[0]).toAbsolutePath()
         println "You gave me the book $book"

         Bookwards bookwards = new Bookwards(book)

         println "Do you want to read (r), question (q) or review (w)? (Press ENTER after selection)"
         BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
         String answer = reader.readLine();
         switch (answer) {
            case "r": bookwards.run(); break;
            case "q": askOrderAndQuestion(book); break;
            case "w": askStateAndReview(book); break;
            default: println "Nope"; break;
         }
      }

      if (args.size() == 2) {
         Path book = Paths.get(args[0]).toAbsolutePath()
         println "You gave me the book $book"

         String status = args[1]

         Bookwards bookwards = new Bookwards(book)
         bookwards.review (status)
      }
   }
}
