package com.gitlab.edufuga.bookwards;

import java.io.IOException;
import java.nio.file.*;

/**
 * Imports a plain text document (aka. book) into the programs folder in the home directory of the user.
 */
public class BookImporter {
    final Path book;
    final String language;
    final Path userHome;

    boolean imported;

    public BookImporter(Path book, String language) {
        this.book = book;
        this.language = language;

        userHome = Paths.get(System.getProperty("user.home"));
        if (Files.notExists(userHome)) {
            throw new RuntimeException("The user.home property is not a valid folder.");
        }
    }

    public Path importBook() throws IOException {
        Path languageFolder = userHome.resolve(".bookwards").resolve(language);

        String bookName = book.getParent().getFileName().toString();
        Path bookFolder = languageFolder.resolve(bookName);
        Files.createDirectories(bookFolder);

        Path importedBook = bookFolder.resolve(book.getFileName().toString());

        if (Files.exists(importedBook)) {
            System.out.println("The book'" + book.toAbsolutePath() + "' was already imported to '" + importedBook.toAbsolutePath() + "'.");
            imported = false;
        }
        else {
            System.out.println("Importing book '" + book.toAbsolutePath() + "' to '" + importedBook.toAbsolutePath() + "'.");
            Files.copy(book, importedBook, StandardCopyOption.COPY_ATTRIBUTES);
            imported = true;
        }
        return importedBook;
    }

    public boolean isImported() {
        return imported;
    }
}
