# Introduction

This is a personal project. It consists of a [computer program](https://gitlab.com/edufuga/bookwards) that I've written in order to learn, improve and mantain foreign languages. More concretely, it helps with the *vocabulary* of a language.

# What can the program do?

The main purpose of the program is to improve your vocabulary of any language you care about by seeing and practising it in context. The context is provided by a book you want to read. There are two main functionalities: *questioning* and *reading*.

First of all, you use the "questioner" to enter your self-estimated knowledge of new words. This looks like the following screen recording:

[![asciicast](https://asciinema.org/a/c21OZ7RaPPqaXhSp6GTP2kdyS.svg)](https://asciinema.org/a/c21OZ7RaPPqaXhSp6GTP2kdyS)

Then, once you've entered a decent amount of vocabulary, you can read the book. That will look like this:

[![asciicast](https://asciinema.org/a/vPcq5J4Q32cVrZrzIUa5tCXQJ.svg)](https://asciinema.org/a/vPcq5J4Q32cVrZrzIUa5tCXQJ)

# Evaluate the program (only for demonstration purposes)

By including my own vocabulary and writings in the Docker image, you can execute the program without taking care of any previous setup (other than having Docker). This way you can get a feel for the program. That said, please note that any changes in the vocabulary will be lost once the program is closed.

To start the console program, run the following command:

    docker run --rm -it edufuga/edwords:latest

It should look similar to [this screen recording](https://asciinema.org/a/xxqfJnbNcAGelmC3TehU1uZhU), but with a different starting text. 
The starting text that I've chosen for demonstrating the program describes the program itself :). 

The colors of the text correspond to my *current* vocabulary knowledge of the corresponding language. Since I wrote the text myself, most of the words are shown in blue (i.e. actively known). Unknown words are shown in red, guessed words are orange, comprehended (understood, i.e. passively known) words are purple. Those words that I've considered as not beloning to a language itself, I've marked as ignored; they have a rose tone.

The program works by pressing single keys of the keyboard. If you're practising vocabulary with the questioner, then the keys I, U, G, C, K for "ignored", "unknown", "guessed", "comprehended" and "known" will be useful for telling the program what's your knowledge of every entered word. If you're reading a book, then you can enter your word knowledge by pressing the E key, change the color mode with F, create connections between previously entered words using the R key, view the previously entered connections with K, navigate foward to a different line in the book by pressing L, show the current line number with S, mark the current screen position with M, returning to the previously marked position with N, clear the screen with C and, finally, quit the program with Q.

# Use the program productively (using your own vocabulary)

If you want to run the program with your *own* vocabulary, then run the following command:

    docker run -v $WORDSFILES:/root/edwordsfiles --rm -it edufuga/edwords

Here I'm assuming that the environment variable `WORDSFILES` was set. In my case, I save the vocabulary files in the directory `/Users/eduard/Develop/WordsFiles`, so the previous command corresponds to the following alternative:

    docker run -v /Users/eduard/Develop/WordsFiles:/root/edwordsfiles --rm -it edufuga/edwords

This way, the vocabulary in the path `/root/edwordsfiles` within the Docker container is replaced (shadowed) by the local folder saved as the value of the variable `WORDSFILES`. Now you're using my program with your own vocabulary, which is properly saved somewhere in your computer, so it won't be lost once you close the program.

# Use the program more productively (using your own vocabulary and reading your own books)

Until now, the program has shown you one of my texts. Obviously, at some point you'll want to read something else. At least, I hope so. You can achieve that by mounting the plain text file you want to read into the Docker container and telling Docker where the book is found inside the container.

For example, in order to read a [book about Docker](https://www.rheinwerk-verlag.de/docker-das-praxisbuch-fuer-entwickler-und-devops-teams/) with the program, using my locally saved vocabulary, I execute the following command:

    docker run -v $WORDSFILES:/root/edwordsfiles -v $EBOOKS/DE/Docker\ -\ Bernd\ Öggl,\ Michael\ Kofler/book.txt:/root/DE/Docker/book.txt -e BOOK=/root/DE/Docker/book.txt --rm -it edufuga/edwords

The program expects the book to be a plain text file. Originally, I converted the .epub file to a .txt file using [Calibre](https://calibre-ebook.com/). I recommend you do the same for the books you want to read.

Like in the previous example, both `WORDSFILES` and `EBOOKS` are environment variables, pointing to the vocabulary and book folders, respectively. Additionally, the environment variable `BOOK` is used to tell Docker where the book file will be found. Obviously, this must correspond to the target of the volume mount (in this case, that's the path `/root/DE/Docker/book.txt`).

**Tip**: The program will import the book every time you start it, so depending on the book's length, that can take a moment. If you want to optimize that, then you should mount the directory `/root/.bookwards` to a local directory, following the same technique as before.
