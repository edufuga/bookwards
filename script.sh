#/bin/bash

set -e

echo "Cleaning"
./gradlew clean

echo ""

echo "Building"
./gradlew --console=plain build

echo "Unpacking"
cd build
cd distributions
unzip -o bookwards.zip

echo "The program was built and unpacked."
echo "Have a nice day."
